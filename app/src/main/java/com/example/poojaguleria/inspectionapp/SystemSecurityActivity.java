package com.example.poojaguleria.inspectionapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class SystemSecurityActivity extends AppCompatActivity {

    private List<Questions> questionsList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private QuesListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_security);

        mRecyclerView = (RecyclerView) findViewById(R.id.question_list);

        setCCTVQuesList();
        setAlarmSystemQuesList();
        setAccessControlSysQuesList();
        setVisitorsVendorsQuesList();
        setVehicleControlQuesList();
        setFireProtectionSysQuesList();
        setPortFireExtQuesList();
        setExitSignDoorQuesList();
        setCashMgmtSysQuesList();
        setInfoSysQuesList();
        setCentralMailHandQuesList();

    }

    private void setCCTVQuesList() {

        mAdapter = new QuesListAdapter(this,questionsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        Questions questions = new Questions("", "CCTV System");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q98.What is the maintenance schedule for the CCTV surveillance system?", "");
        questionsList.add(questions);
        questions = new Questions("Q99.Are CCTV images stored and kept in accordance with the evidential needs of the police, minimum 30 days and available on demand?", "");
        questionsList.add(questions);
        questions = new Questions("Q100.Is the CCTV system integrated with video analytics? If yes, please describe features available", "");
        questionsList.add(questions);
        questions = new Questions("Q101. Are the CCTV images clear and provide the following function?\n" +
                "a)\tDetection\n" +
                "b)\tObservation\n" +
                "c)\tRecognition of a known person\n" +
                "d)\tIdentification of an unknown person", "");
        questionsList.add(questions);
        questions = new Questions("Q102.Are the date and time stamps of the system accurate and synchronized?", "");
        questionsList.add(questions);
        questions = new Questions("Q103.Are the CCTV cameras and system tamper-proof?", "");
        questionsList.add(questions);
        questions = new Questions("Q104.Can the CCTV system be viewed remotely off-site? ", "");
        questionsList.add(questions);
        questions = new Questions("Q105.Is the camera recording on local DVR/NVR or Network Cloud?", "");
        questionsList.add(questions);
        questions = new Questions("Q106.Is there any system prompt on recording failure or system failure?", "");
        questionsList.add(questions);
        questions = new Questions("Q107.When was the last breakdown and cause?", "");
        questionsList.add(questions);
        questions = new Questions("Q108.Where are the recorders located?", "");
        questionsList.add(questions);
        questions = new Questions("Q109.What are the access control measures for recorders?", "");
        questionsList.add(questions);
        questions = new Questions("Q110.Are there any back-ups of CCTV recordings maintained?", "");
        questionsList.add(questions);
        questions = new Questions("Q111.What are the control measures to prevent authorised access to back-ups of CCTV recordings?", "");
        questionsList.add(questions);
        questions = new Questions("Q112.Is access to the CCTV system restricted by password?", "");
        questionsList.add(questions);
        questions = new Questions("Q113.How often is the password changed?", "");
        questionsList.add(questions);
        mAdapter.notifyDataSetChanged();
    }

    private void setAlarmSystemQuesList() {

        Questions questions = new Questions("", "Alarm System");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q114.Are the buildings secured with intrusion alarm system?", "");
        questionsList.add(questions);
        questions = new Questions("Q115.Is the system tested frequently?", "");
        questionsList.add(questions);
        questions = new Questions("Q116.Is the alarm connected to any central monitoring station (CMS)?", "");
        questionsList.add(questions);
        questions = new Questions("Q117.Is there an established list of Responsible Officer (RO) to be activated / alerted by CMS?", "");
        questionsList.add(questions);
        questions = new Questions("Q118.How often is the list reviewed?", "");
        questionsList.add(questions);
        questions = new Questions("Q119.Are there reports of false alarm activation? Frequency?\n" +
                "Reason for false alarm activation and any corrective measures implemented.\n", "");
        questionsList.add(questions);
        questions = new Questions("Q120.Is there any Alarm Response Plan? ", "");
        questionsList.add(questions);
        questions = new Questions("Q121.What is the response time for alarm activation?", "");
        questionsList.add(questions);
        questions = new Questions("Q122.Is there any alarm activation exercise conducted? When was it last conducted?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setAccessControlSysQuesList() {

        Questions questions = new Questions("", "Access Control System");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q123.Are employees issued with an identification badge?", "");
        questionsList.add(questions);
        questions = new Questions("Q124.Are identification badges prominently displayed?", "");
        questionsList.add(questions);
        questions = new Questions("Q125.Are the badges differentiated for different work areas?", "");
        questionsList.add(questions);
        questions = new Questions("Q126.What action is taken for non-compliance?", "");
        questionsList.add(questions);
        questions = new Questions("Q127.Is there any special identification for employees entering any restricted area?", "");
        questionsList.add(questions);
        questions = new Questions("Q128.What is the procedure for loss of identification badge?", "");
        questionsList.add(questions);
        questions = new Questions("Q129.What is the procedure for the issue or replacement of identification badge?", "");
        questionsList.add(questions);
        questions = new Questions("Q130.Is the issuance of Employee’s Identification Badge controlled?", "");
        questionsList.add(questions);
        questions = new Questions("Q131.What is the procedures for recovery and invalidation of identification badge for employees that are terminated or resigned from service?", "");
        questionsList.add(questions);
        questions = new Questions("Q132.Is Security notified of Lost, Expired or Invalid passes?", "");
        questionsList.add(questions);
        questions = new Questions("Q133.Is Security notified is a staff is on leave?", "");
        questionsList.add(questions);
        questions = new Questions("Q134.Is employee access suspended when on leave?", "");
        questionsList.add(questions);
        mAdapter.notifyDataSetChanged();
    }

    private void setVisitorsVendorsQuesList() {

        Questions questions = new Questions("", "Visitors & Vendors");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q135.Are visitors and vendors issued with a visitor badge with serial number and records kept?", "");
        questionsList.add(questions);
        questions = new Questions("Q136.Are the badges for visitors, vendors and employees differentiated by colour, design, etc?", "");
        questionsList.add(questions);
        questions = new Questions("Q137.Are badges recorded and controlled by rigid accountability procedures?", "");
        questionsList.add(questions);
        questions = new Questions("Q138.Are details of visit and particulars of visitors / vendors recorded?", "");
        questionsList.add(questions);
        questions = new Questions("Q139.Is time of departure recorded of visit recorded?", "");
        questionsList.add(questions);
        questions = new Questions("Q140.Are visitor / vendors and / or their belongings search before entry?", "");
        questionsList.add(questions);
        questions = new Questions("Q141.Is a notice on search prominently display?", "");
        questionsList.add(questions);
        questions = new Questions("Q142.Is a person search or procedure available?", "");
        questionsList.add(questions);
        questions = new Questions("Q143.Describe search procedure adopted for search of persons and belongings?", "");
        questionsList.add(questions);
        questions = new Questions("Q144.Are security officers trained on identification of suspicious persons and suspicious belongings?", "");
        questionsList.add(questions);
        questions = new Questions("Q145.Are security officers trained on search of persons and belongings?", "");
        questionsList.add(questions);
        questions = new Questions("Q146.Is there a Visitors’ Management System (VMS) in use?", "");
        questionsList.add(questions);
        questions = new Questions("Q147.Is there any regular maintenance regime for VMS?", "");
        questionsList.add(questions);
        questions = new Questions("Q148.Is there a separate system for visitors and vendors?", "");
        questionsList.add(questions);
        questions = new Questions("Q149.Are vendors verified prior to engagement?", "");
        questionsList.add(questions);
        questions = new Questions("Q150.Are vendors being escorted to their respective areas by employee or security?", "");
        questionsList.add(questions);
        questions = new Questions("Q151.Is there a visitor escort procedure established other than to the wards?", "");
        questionsList.add(questions);
        questions = new Questions("Q152.Are there checks on visitors' movements to prevent access to unauthorised areas?", "");
        questionsList.add(questions);
        questions = new Questions("Q153.Who are issued with admin rights to access the system?", "");
        questionsList.add(questions);
        questions = new Questions("Q154.How frequently is the password updated?", "");
        questionsList.add(questions);
        questions = new Questions("Q155.In the event of an emergency, is the system able to indicate who is still within the premises?", "");
        questionsList.add(questions);
        questions = new Questions("Q156.What is the procedure to ensure that no visitor or vendors had overstayed their visit?", "");
        questionsList.add(questions);
        questions = new Questions("Q157.In the event of heightened security what is the time taken to implement MOH Level 1 & 2 security measures in relation to access control and checks?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }
    private void setVehicleControlQuesList() {

        Questions questions = new Questions("", "Vehicle Control");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q158.Are procedures used for control of special vehicles?\n" +
                "a)\tAmbulances\n" +
                "b)\tEmergency vehicles\n" +
                "c)\tVIP vehicles\n" +
                "d)\tStaff vehicles\n" +
                "e)\tDelivery vehicles\n" +
                "f)\tVehicles with loads which may be impractical to search\n", "");
        questionsList.add(questions);
        questions = new Questions("Q159.Are there restricted / private parking areas not open to public?", "");
        questionsList.add(questions);
        questions = new Questions("Q160.Are authorised vehicles issued with car labels, decals or transponders? If others, please provide details", "");
        questionsList.add(questions);
        questions = new Questions("Q161.Are car labels, decals or transponders returned when vehicle is no longer authorised to enter the facility?", "");
        questionsList.add(questions);
        questions = new Questions("Q162.Are vehicles with regular access to the hospital registered with the security department?", "");
        questionsList.add(questions);
        questions = new Questions("Q163.Is a vehicle search procedure available?", "");
        questionsList.add(questions);
        questions = new Questions("Q164.Describe vehicle search procedure adopted", "");
        questionsList.add(questions);
        questions = new Questions("Q165.Are security officers trained on vehicle search procedures?", "");
        questionsList.add(questions);
        questions = new Questions("Q166.Are the security officers trained on identification of suspicious vehicles?", "");
        questionsList.add(questions);
        questions = new Questions("Q167.What are the procedures for registration of private cars in restricted carparks or areas?", "");
        questionsList.add(questions);
        questions = new Questions("Q168.In the event of heightened security what is the time taken to implement MOH Level 1 & 2 security measures in relation to vehicle control and checks?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }
    private void setFireProtectionSysQuesList() {

        Questions questions = new Questions("", "Fire Protection Systems ");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q169.Where is the FCC located? Is it manned?", "");
        questionsList.add(questions);
        questions = new Questions("Q170.Are the officers manning the Fire Command Centre trained? What are the nature of training attended?", "");
        questionsList.add(questions);
        questions = new Questions("Q171.Are there frequent activations of false fire alarm?", "");
        questionsList.add(questions);
        questions = new Questions("Q172.Is your fire protection system regularly serviced in accordance with standard code of practice for fire alarm system?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }
    private void setPortFireExtQuesList() {

        Questions questions = new Questions("", "PORTABLE FIRE EXTINGUISHER");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q173.a) Properly hung on bracket\t\t\t\n" +
                "\n" +
                "b) Clear of obstruction\t\t\t\n" +
                "\n" +
                "c) Serviced by licensed contractor half-yearly\t\t\t\n" +
                "\n" +
                "d) With PSB label\n", "");
        questionsList.add(questions);
        questions = new Questions("Q174.Are security officers trained on basic fire fighting and the use of fire extinguishers?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }
    private void setExitSignDoorQuesList() {

        Questions questions = new Questions("", "EXIT SIGN AND DOORS");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q175.a) Exit sign provided along staircases, escape routes and public areas\t\t\t\n" +
                "\n" +
                "b) Exit sign visible\t\t\t\n" +
                "\n" +
                "c) Directional exit sign provided where necessary\t\t\n" +
                "\n" +
                "d) Exit sign operation in order\n", "");
        questionsList.add(questions);
        questions = new Questions("Q176.a) Exit doors are unlocked (EM locks are released) during fire emergencies\n" +
                "\n" +
                "b) Exit doors and staircases are clear of obstruction\tand fire hazards\n", "");


        mAdapter.notifyDataSetChanged();
    }
    private void setCashMgmtSysQuesList() {

        Questions questions = new Questions("", "Cash Management Systems ");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q177.How are cash transactions managed? Any procedures on handling cash?", "");
        questionsList.add(questions);
        questions = new Questions("Q178.Are cash kept overnight?", "");
        questionsList.add(questions);
        questions = new Questions("Q179.How are cash kept on site? ", "");
        questionsList.add(questions);
        questions = new Questions("Q180.What measures are in place to safeguard cash kept on site?", "");
        questionsList.add(questions);
        questions = new Questions("Q181.Are cash counters monitored by CCTV or distress button?", "");
        questionsList.add(questions);
        questions = new Questions("Q182.What are the measures to prevent theft / robbery, etc.", "");
        questionsList.add(questions);
        questions = new Questions("Q183.Is there any security escort provided for Cash-in-Transit?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }
    private void setInfoSysQuesList() {

        Questions questions = new Questions("", "Information System ");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q184.Is there a secured server room on site with restricted access?", "");
        questionsList.add(questions);
        questions = new Questions("Q185.Is there a secondary data storage off-site?", "");
        questionsList.add(questions);
        questions = new Questions("Q186.Is access to information restricted on a need-to-know basis?", "");
        questionsList.add(questions);
        questions = new Questions("Q187.Are computers protected by password, firewall, etc?", "");
        questionsList.add(questions);
        questions = new Questions("Q188.Are there any alerts on unauthorised access to computers and other systems?", "");
        questionsList.add(questions);
        questions = new Questions("Q189.What is the security response procedure when unauthorised access is detected?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }
    private void setCentralMailHandQuesList() {

        Questions questions = new Questions("", "Central Mail Handling Facility ");
        questions.setHeader(true);

        questions = new Questions("Q190.Is there a Central Mail Handling room or facility?", "");
        questionsList.add(questions);
        questions = new Questions("Q191.Is the access to the mail room restricted and secured?", "");
        questionsList.add(questions);
        questions = new Questions("Q192.Describe access control measures to mail room", "");
        questionsList.add(questions);
        questions = new Questions("Q193.Is there a designated area for the opening of mails?", "");
        questionsList.add(questions);
        questions = new Questions("Q194.Is there a procedure for isolating of suspicious mails?", "");
        questionsList.add(questions);
        questions = new Questions("Q195.Is there a designated area to isolate suspicious mails?", "");
        questionsList.add(questions);
        questions = new Questions("Q196.Are mail room personnel trained on identification of suspicious mails / parcels?", "");
        questionsList.add(questions);
        questions = new Questions("Q197.In the event of heightened security what is the time taken to implement MOH Level 1 & 2 security measures in relation to screening of mails?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }
}
