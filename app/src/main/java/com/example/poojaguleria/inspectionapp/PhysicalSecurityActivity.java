package com.example.poojaguleria.inspectionapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PhysicalSecurityActivity extends AppCompatActivity {

    private List<Questions> questionsList = new ArrayList<>();
    private final int CAMERA_REQUEST = 1001;
    private RecyclerView genrlEnvRecyclerView;
    private QuesListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_physical_security);

        genrlEnvRecyclerView = (RecyclerView) findViewById(R.id.question_list);
        Button submitButton = (Button) findViewById(R.id.submit_btn);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                startActivityForResult(cameraIntent, CAMERA_REQUEST);*/
                callApi();
            }
        });

        setGenrlEnvQuesList();
        setStructureQuesList();
        setBarriersQuesList();
        setLightingsQuesList();
        setCarparksQuesList();
        setPublicLinksQuesList();
        setBuildingInternalQuesList();
        setEmergencyDeptQuesList();
        setWardsQuesList();
        setRestrictedAreaQuesList();
        setRestrictedAreaQuesList2();


    }

    private void callApi() {
        JSONObject data = new JSONObject();
        JSONArray formdata = new JSONArray();
        JSONObject jsonObject1 = null;
        JSONArray questionsArray = null;
        for (int i = 0; i < questionsList.size(); i++) {
            try {
                if (questionsList.get(i).isHeader()) {
                    if(questionsArray!=null){
                        jsonObject1.put("questions",questionsArray);
                        formdata.put(jsonObject1);
                    }
                    jsonObject1 = new JSONObject();
                    jsonObject1.put("subcategory", questionsList.get(i).getHeader());
                    questionsArray = new JSONArray();
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("question", questionsList.get(i).getQuestion());
                    jsonObject.put("yes", questionsList.get(i).isYes() ? "1" : "0");
                    jsonObject.put("na", questionsList.get(i).isNa() ? "1" : "0");
                    jsonObject.put("no", questionsList.get(i).isNo() ? "1" : "0");
                    jsonObject.put("comments", questionsList.get(i).getComments());
                    questionsArray.put(jsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {

            data.put("formdata",formdata);
            JSONObject mainObject=new JSONObject();
            mainObject.put("imageUpload","0");
            mainObject.put("inspection_id","1");
            mainObject.put("data",data);
            Log.e("value",mainObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateList(List<Questions> questionsList) {
        this.questionsList = questionsList;
    }

    private void setStructureQuesList() {

        Questions questions = new Questions("", "Building Structure & External Perimeter ");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q9.Describe Building structure, e.g. concrete walls, glass windows, etc.", "");
        questionsList.add(questions);
        questions = new Questions("Q10.Any use of bollards or structures, e.g. street furniture, natural barriers such as shrubs, bushes, drains use to create stand-off distance or prevent vehicle ramming?", "");
        questionsList.add(questions);
        questions = new Questions("Q11.Is the glass structure protected against blast / explosion?", "");
        questionsList.add(questions);
        questions = new Questions("Q12.Are openings such as culverts, tunnels, manholes for sewers and utility access, and sidewalk elevators which permit access to the facility secured?", "");
        questionsList.add(questions);
        questions = new Questions("Q13.Any security patrols conducted at the external areas? If, yes, what is the frequency?", "");
        questionsList.add(questions);
        questions = new Questions("Q14.Do you conduct patrol using robot or are planning to do so in future?", "");
        questionsList.add(questions);
        questions = new Questions("Q15.Are you using drones for external patrol, if not, do you have any plan of using it in future?", "");
        questionsList.add(questions);
        questions = new Questions("Q16.Is the external environment monitored by CCTV cameras?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setGenrlEnvQuesList() {

        mAdapter = new QuesListAdapter(this, questionsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        genrlEnvRecyclerView.setLayoutManager(mLayoutManager);
        genrlEnvRecyclerView.setAdapter(mAdapter);

        Questions questions = new Questions("", "General Environment");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q1.Address and Location of Hospital", "");
        questionsList.add(questions);
        questions = new Questions("Q2.Land Area / Gross Floor Area", "");
        questionsList.add(questions);
        questions = new Questions("Q3.Buildings / facilities within immediate vicinity", "");
        questionsList.add(questions);
        questions = new Questions("Q4.Nearby MRT", "");
        questionsList.add(questions);
        questions = new Questions("Q5.Average site population", "");
        questionsList.add(questions);
        questions = new Questions("Q6.Number of blocks / floors", "");
        questionsList.add(questions);
        questions = new Questions("Q7.Number of wards / beds", "");
        questionsList.add(questions);
        questions = new Questions("Q8.List critical assets, e.g. Water Tank, AHU, Electrical Sub-station, etc.", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setBarriersQuesList() {

        Questions questions = new Questions("", "Barriers, Fencing & Signage ");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q17.Describe type of barriers, including natural barriers or perimeter fencing at hospital boundary?", "");
        questionsList.add(questions);
        questions = new Questions("Q18.Any checks conducted on perimeter border? How often?", "");
        questionsList.add(questions);
        questions = new Questions("Q19.Are irregularities reported? (e.g. damage, malfunction, etc.)", "");
        questionsList.add(questions);
        questions = new Questions("Q20.Is there security signage posted at entrances and other areas to warn and deter offenders?", "");
        questionsList.add(questions);
        questions = new Questions("Q21.Is signage displayed clear and visible to deter risks and threats?", "");
        questionsList.add(questions);
        questions = new Questions("Q22.Is there adequate signage to provide information and directions and prevent and deter access to controlled areas?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setLightingsQuesList() {


        Questions questions = new Questions("", "Lightinings");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q23.Are there adequate lighting to provide good visibility and support CCTV operations that facilitates facial and other identification, description, etc.", "");
        questionsList.add(questions);
        questions = new Questions("Q24.Are the external areas well-lighted?", "");
        questionsList.add(questions);
        questions = new Questions("Q25.Any external areas found dark or with inadequate lighting?", "");
        questionsList.add(questions);
        questions = new Questions("Q26.Are the internal areas well-lighted?", "");
        questionsList.add(questions);
        questions = new Questions("Q27.Are internal areas found dark or with inadequate lighting?", "");
        questionsList.add(questions);
        questions = new Questions("Q28.How often are checks conducted that lights are working?", "");
        questionsList.add(questions);
        questions = new Questions("Q29.Are repairs to lights and replacement of inoperative lamps effected immediately?", "");
        questionsList.add(questions);
        questions = new Questions("Q30.Is the power supply for lights adequately protected?", "");
        questionsList.add(questions);
        questions = new Questions("Q31.Is there provision for standby or emergency lighting", "");
        questionsList.add(questions);
        questions = new Questions("Q32.Is the standby or emergency equipment tested frequently?", "");
        questionsList.add(questions);
        questions = new Questions("Q33.Are there adjacent street lights, portable lights, torchlights, etc available to provide lightings in the event of lighting failures?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setCarparksQuesList() {

        Questions questions = new Questions("", "Carparks");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q34.Are carparks provided within the hospital grounds?", "");
        questionsList.add(questions);
        questions = new Questions("Q35.Are vehicle permitted to park along driveways, wait at the roadside, etc.", "");
        questionsList.add(questions);
        questions = new Questions("Q36.What actions are taken for illegal or indiscriminate parking?", "");
        questionsList.add(questions);
        questions = new Questions("Q37.Under what circumstances are security checks conducted on vehicle upon entry?", "");
        questionsList.add(questions);
        questions = new Questions("Q38.Any security patrol conducted at carpark? If yes, what is the frequency?", "");
        questionsList.add(questions);
        questions = new Questions("Q39.Is the carpark protected by CCTV cameras?", "");
        questionsList.add(questions);
        questions = new Questions("Q40.Is the lighting in the carpark adequate to deter and detect potential threats such as theft, mischief or other security incidents?", "");
        questionsList.add(questions);
        questions = new Questions("Q41.Is Licence Plate Recognition System implemented at carpark?", "");
        questionsList.add(questions);
        questions = new Questions("Q42.Any system alerts if car has been left abandoned / parking at the carpark for a prolong period?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setPublicLinksQuesList() {


        Questions questions = new Questions("", "Public Links, Walkways and Retail Areas ");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q43.Are public links, walkways and retail areas monitored by CCTV cameras?", "");
        questionsList.add(questions);
        questions = new Questions("Q44.Are security patrols conducted at public links, walkways and retails area? If yes, what is the frequency of patrol?", "");
        questionsList.add(questions);
        questions = new Questions("Q45.Are there doors, gates, etc. that are locked and secured to prevent access during restricted hours? ", "");
        questionsList.add(questions);
        questions = new Questions("Q46.Is the locking mechanism activated during emergency lockdown?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setWardsQuesList() {

        Questions questions = new Questions("", "Wards and Consultation Rooms");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q47.Are the entrances to the building interior controlled?", "");
        questionsList.add(questions);
        questions = new Questions("Q48.Describe locking mechanism, e.g. manual locks and keys, card access, biometric, etc., if any", "");
        questionsList.add(questions);
        questions = new Questions("Q49.Are doors, windows, roller shutters, turnstiles, etc. locked and secured or controlled to prevent unauthorised access during non-operational hours?", "");
        questionsList.add(questions);
        questions = new Questions("Q50.Are there any CCTV cameras deployed to monitor entrances?", "");
        questionsList.add(questions);
        questions = new Questions("Q51.Are there CCTV cameras at internal public areas and waiting areas?", "");
        questionsList.add(questions);
        questions = new Questions("Q52.Any security patrols conducted, if yes what is the frequency?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setEmergencyDeptQuesList() {

        Questions questions = new Questions("", "Emergency Department");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q53.Any security officer deployed at the Emergency Department? How many officers are deployed?", "");
        questionsList.add(questions);
        questions = new Questions("Q54.Is the security officer attired in security uniform?", "");
        questionsList.add(questions);
        questions = new Questions("Q55.Does the security officer / Auxiliary Police Officer carry any firearm?", "");
        questionsList.add(questions);
        questions = new Questions("Q56.What equipment is available to the security officer to protect himself against acts of violence?", "");
        questionsList.add(questions);
        questions = new Questions("Q57.Is the officer equipped with communication set to report incident or call for assistance?", "");
        questionsList.add(questions);
        questions = new Questions("Q6.Number of blocks / floors", "");
        questionsList.add(questions);
        questions = new Questions("Q58.Is the security officer equipped with body cam?", "");
        questionsList.add(questions);
        questions = new Questions("Q59.Is the security officer equipped with any “distress call” to alert security control?", "");
        questionsList.add(questions);
        questions = new Questions("Q60.Does he have a whistle?", "");
        questionsList.add(questions);
        questions = new Questions("Q61.Is he trained to deal with threatening, disorderly or violent persons?", "");
        questionsList.add(questions);
        mAdapter.notifyDataSetChanged();
    }

    private void setBuildingInternalQuesList() {

        Questions questions = new Questions("", "Building Internal");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q62.Is access to the consultation rooms controlled?", "");
        questionsList.add(questions);
        questions = new Questions("Q63.Is there a Visitors’ Management System (VMS) for visitors to the ward?", "");
        questionsList.add(questions);
        questions = new Questions("Q64.Is the VMS manual or computerised?", "");
        questionsList.add(questions);
        questions = new Questions("Q65.Under what circumstances are checks conducted on visitors / patients and their belongings? ", "");
        questionsList.add(questions);
        questions = new Questions("Q66.Are patients advised not to keep their valuables in the hospitals?", "");
        questionsList.add(questions);
        questions = new Questions("Q67.Are lockers provided for patients to safekeep their belongings? ", "");
        questionsList.add(questions);
        questions = new Questions("Q68.If lockers are provided, what are the security measures to safeguard patient’s belongings?", "");
        questionsList.add(questions);
        questions = new Questions("Q69.Are there any CCTV cameras deployed to monitor general ward areas and entrances?", "");
        questionsList.add(questions);
        questions = new Questions("Q70.Any security patrols conducted, if yes what is the frequency?", "");
        questionsList.add(questions);
        questions = new Questions("Q71.Any panic buttons available to alert security of incidents in the ward or consultation room?", "");
        questionsList.add(questions);
        questions = new Questions("Q72.Pending security response, what measures will be undertaken by staff, if any to contain the situation?", "");
        questionsList.add(questions);
        questions = new Questions("Q73.Is there a separate or designated room provided for persons in police custody seeking treatment?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setRestrictedAreaQuesList() {

        Questions questions = new Questions("", "Restricted Areas");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q74.Describe access control measures to Critical Assets (Water Tank, AHU, IT Server, etc.), e.g. manual locks and keys, card access, biometric, etc., if any", "");
        questionsList.add(questions);
        questions = new Questions("Q75.Who will be granted access to such facilities?", "");
        questionsList.add(questions);
        questions = new Questions("Q76.What measures are taken prior to granting approval / access to staff?", "");
        questionsList.add(questions);
        questions = new Questions("Q77.Is the access given to authorised person reviewed periodically? If yes, how frequent is the review?", "");
        questionsList.add(questions);
        questions = new Questions("Q78.If an authorised person has resigned or left the hospital, how soon will his access be removed?", "");
        questionsList.add(questions);
        questions = new Questions("Q79.Is the person entering the facility checked, escorted and recorded?", "");
        questionsList.add(questions);
        questions = new Questions("Q80.Describe access control measures to Nursery, e.g. manual locks and keys, card access, biometric, etc., if any", "");
        questionsList.add(questions);
        questions = new Questions("Q81.Describe access control measures to Pharmacy, e.g. manual locks and keys, card access, biometric, etc., if any", "");
        questionsList.add(questions);
        questions = new Questions("Q82.Describe access control measures to Mortuary, e.g. manual locks and keys, card access, biometric, etc., if any", "");
        questionsList.add(questions);
        questions = new Questions("Q83.Describe access control measures to offices and Staff areas, e.g. manual locks and keys, card access, biometric, etc., if any", "");
        questionsList.add(questions);
        questions = new Questions("Q84.Are there any CCTV cameras deployed to monitor restricted areas?", "");
        questionsList.add(questions);
        questions = new Questions("Q85.Is there any alert of unauthorised access to restricted areas?", "");
        questionsList.add(questions);
        questions = new Questions("Q86.What is the security response when there is a detection of unauthorised access to restricted areas?", "");
        questionsList.add(questions);
        questions = new Questions("Q87.Any security patrols conducted, if yes what is the frequency?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setRestrictedAreaQuesList2() {
        Questions questions = new Questions("", "Restricted Areas (Security Office / Control Room / FCC)");
        questions.setHeader(true);

        questions = new Questions("Q88.Describe access control measures to Security Office", "");
        questionsList.add(questions);
        questions = new Questions("Q89.Describe access control measures to Security Control Room. Is access given to all security personnel?", "");
        questionsList.add(questions);
        questions = new Questions("Q90.Describe access control measures to FCC", "");
        questionsList.add(questions);
        questions = new Questions("Q91.Is the door locked and secured?", "");
        questionsList.add(questions);
        questions = new Questions("Q92.Is the Security Office / Control Room / FCC manned at all times?", "");
        questionsList.add(questions);
        questions = new Questions("Q93.Is the Security Office / Control Room / FCC exposed to public view?", "");
        questionsList.add(questions);
        questions = new Questions("Q94.What is the main communication mode for Control Room / FCC?", "");
        questionsList.add(questions);
        questions = new Questions("Q95.Is there alternative communication mode when the main mode is not functioning?", "");
        questionsList.add(questions);
        questions = new Questions("Q96.Any critical contact number to notify when Control Room / FCC is compromised?", "");
        questionsList.add(questions);
        questions = new Questions("Q97.Any neighbouring facilities available to be converted as Control Room / FCC?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }


}

