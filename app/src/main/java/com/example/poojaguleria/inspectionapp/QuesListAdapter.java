package com.example.poojaguleria.inspectionapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pooja Guleria on 22-07-2018.
 */

public class QuesListAdapter extends RecyclerView.Adapter<QuesListAdapter.MyViewHolder> {

    List<Questions> questionsList;
    private Context context;
    public QuesListAdapter(Context context,List<Questions> questionsList) {
        this.questionsList = questionsList;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.qestion_row_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Questions questions = questionsList.get(position);
        //  holder.quesNo.setText("Q"+questions.getQuesNo());
        if (questions.isHeader()) {
            holder.listSection.setVisibility(View.GONE);
            holder.header.setVisibility(View.VISIBLE);
            holder.heading.setText(questions.getHeader());

        } else {
            holder.header.setVisibility(View.GONE);
            holder.listSection.setVisibility(View.VISIBLE);
            holder.ques.setText(questions.getQuestion());
            holder.yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        questionsList.get(position).setYes(true);

                    } else
                        questionsList.get(position).setYes(false);

                    updateActivityList(context);
                }
            });

            holder.nA.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        questionsList.get(position).setNa(true);

                    } else
                        questionsList.get(position).setNa(false);

                    updateActivityList(context);
                }
            });

            holder.no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        questionsList.get(position).setYes(true);

                    } else
                        questionsList.get(position).setYes(false);

                    updateActivityList(context);

                }
            });

            holder.comments.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    questionsList.get(position).setComments(charSequence.toString());
                    updateActivityList(context);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        }
    }

    private void updateActivityList(Context context){
        if(context instanceof PhysicalSecurityActivity){
            ((PhysicalSecurityActivity)context).updateList(questionsList);
        }
    }

    @Override
    public int getItemCount() {
        return questionsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ques;
        public LinearLayout header;
        public LinearLayout listSection;
        public CheckBox yes;
        public CheckBox no;
        public CheckBox nA;
        public EditText comments;
        public TextView heading;

        public MyViewHolder(View view) {
            super(view);
            ques = (TextView) view.findViewById(R.id.ques);
            header = (LinearLayout) view.findViewById(R.id.heading_layout);
            listSection = (LinearLayout) view.findViewById(R.id.list_section);
            yes = (CheckBox) view.findViewById(R.id.yes);
            no = (CheckBox) view.findViewById(R.id.no);
            nA = (CheckBox) view.findViewById(R.id.NA);
            heading = (TextView) view.findViewById(R.id.heading);
            comments = (EditText) view.findViewById(R.id.comments);
        }
    }
}
