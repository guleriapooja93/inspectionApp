package com.example.poojaguleria.inspectionapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ManpowerActivity extends AppCompatActivity {

    private List<Questions> questionsList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private QuesListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manpower);

        mRecyclerView = (RecyclerView) findViewById(R.id.question_list);

        setSecurityOffQuesList();
        setSecurSupQuesList();
        setOtherStaffQuesList();
        setTenantsVendorsQuesList();
        setEmgncyRespQuesList();

    }
    private void setSecurityOffQuesList() {

        mAdapter = new QuesListAdapter(this,questionsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        Questions questions = new Questions("", "Security Officers");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q198.Are the security officers deployed in-house or outsourced?", "");
        questionsList.add(questions);
        questions = new Questions("Q199.What is the strength of the Security Department?", "");
        questionsList.add(questions);
        questions = new Questions("Q200.What is the deployment of security officers for the respective shifts? 8hrs or 12 hours?", "");
        questionsList.add(questions);
        questions = new Questions("Q201.Apart from access control, checks and patrols, what other duties are expected of security officers working at the hospital, e.g. concierge, mortuary, lost & found counter, etc?", "");
        questionsList.add(questions);
        questions = new Questions("Q202.Any security patrols conducted at the external areas? If, yes, what is the frequency?", "");
        questionsList.add(questions);
        questions = new Questions("Q203.Are the security officers trained in the following licensing modules?\n" +
                "Provide guard and patrol services\n" +
                "Handle security incidents and services\n" +
                "Recognise Terrorist Threats\n", "");
        questionsList.add(questions);
        questions = new Questions("Q204.Are the security officers trained on First Aid and CPR?", "");
        questionsList.add(questions);
        questions = new Questions("Q205.Are the security officers trained on Basic Fire Fighting?", "");
        questionsList.add(questions);
        questions = new Questions("Q206.Are the security officers trained to deal with terror-related situations?", "");
        questionsList.add(questions);
        questions = new Questions("Q207.Have the security officers attended training on SGSecure? Project Guardian?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setSecurSupQuesList() {

        Questions questions = new Questions("", "Security Supervisors / Team Leader / Manager");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q208.What other courses have the security personnel attended?", "");
        questionsList.add(questions);
        questions = new Questions("Q209.Any drills or exercises conducted to deal with terror-related situations?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setOtherStaffQuesList() {

        Questions questions = new Questions("", "Other Staff (Non-Security)");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q210.Is the Supervisor / Team Leader / Manager employed in-house or outsourced?", "");
        questionsList.add(questions);
        questions = new Questions("Q211.Is the Supervisor / Team Leader / Manager licensed by Police Licensing Regulatory Department?", "");
        questionsList.add(questions);
        questions = new Questions("Q212.Has the Supervisor / Team Leader / Manager completed the prerequisite WSQ security training?", "");
        questionsList.add(questions);
        questions = new Questions("Q213.Is the Supervisor / Team Leader / Manager trained on Crisis management?", "");
        questionsList.add(questions);
        questions = new Questions("Q214.Is there on-going security training (e.g. standard operating procedures, crime prevention, anti-terrorism, patrol techniques, handle security incidents and services, executive protection services, crowd and traffic controls)", "");
        questionsList.add(questions);
        questions = new Questions("Q215.Is contact list of all security personnel updated and available", "");
        questionsList.add(questions);
        questions = new Questions("Q216.Is there an emergency call tree to activate security personnel?", "");
        questionsList.add(questions);


        mAdapter.notifyDataSetChanged();
    }

    private void setTenantsVendorsQuesList() {

        Questions questions = new Questions("", "Tenants, Vendors & Contractors");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q217.Provide list of security equipment available to security officers, e.g. communication set, torch lights, etc.", "");
        questionsList.add(questions);
        questions = new Questions("Q218.Are security personnel attired in uniform when on duty?", "");
        questionsList.add(questions);
        questions = new Questions("Q219.Is crime prevention / security awareness topics included in staff orientation and induction programmes?", "");
        questionsList.add(questions);
        questions = new Questions("Q220.Are non-security personnel trained or briefed on security-related matters? ", "");
        questionsList.add(questions);
        questions = new Questions("Q221.What security courses are attended by non-security personnel?", "");
        questionsList.add(questions);
        questions = new Questions("Q222.How frequent is the security awareness training attended by non-security personnel?", "");
        questionsList.add(questions);
        questions = new Questions("Q223.What security outreach programmes are initiated for non-security personnel, online learning, brochures, posters?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }
    private void setEmgncyRespQuesList() {

        Questions questions = new Questions("", "Emergency Response Team");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q224.Are tenants, vendors and contractors briefed on crime prevention / security awareness topics?", "");
        questionsList.add(questions);
        questions = new Questions("Q225.Are tenants, vendors and contractors trained on security awareness and SGSecure initiatives?", "");
        questionsList.add(questions);
        questions = new Questions("Q226.Are tenants involved in security emergency drills and exercises?", "");
        questionsList.add(questions);
        questions = new Questions("Q227.Is there a designated Crisis Management Team? If yes, provide Organisation Chart and Contact List", "");
        questionsList.add(questions);
        questions = new Questions("Q228.Is there a dedicated emergency response team to attend to fire emergency?", "");
        questionsList.add(questions);
        questions = new Questions("Q229.Is there a dedicated emergency response team to response to terror-related emergency? ", "");
        questionsList.add(questions);
        questions = new Questions("Q230.What are the training provided to emergency response team officers?", "");
        questionsList.add(questions);
        questions = new Questions("Q231.Any drills or exercises conducted on fire or other emergency situations?", "");
        questionsList.add(questions);
        questions = new Questions("Q232.In the event of heightened security what is the time taken to implement MOH Level 1 & 2 security measures in relation to security patrols and integration of security deployment with the police?", "");
        questionsList.add(questions);
        mAdapter.notifyDataSetChanged();
    }

}
