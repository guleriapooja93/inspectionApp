package com.example.poojaguleria.inspectionapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    private EditText userNameEditText;
    private EditText paswdEditText;
    private TextView userNameText;
    private TextView pswdText;
    private String url = "https://www.livesensorspi.com/projects/wp-json/inspection/login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button loginButton = (Button) findViewById(R.id.login_button);
        userNameEditText = (EditText) findViewById(R.id.edit_text_username);
        paswdEditText = (EditText) findViewById(R.id.edit_text_password);
        userNameText = (TextView) findViewById(R.id.text_username);
        pswdText = (TextView) findViewById(R.id.text_password);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLogin();

            }
        });

        userNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (userNameEditText.getText().length() > 0) {
                    userNameText.setVisibility(View.VISIBLE);
                } else {
                    userNameText.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        paswdEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (paswdEditText.getText().length() > 0) {
                    pswdText.setVisibility(View.VISIBLE);
                } else {
                    pswdText.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private boolean isValidPassword(String pswd) {
        Matcher matcher = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{4,20})").matcher(pswd);
        return matcher.matches();

    }

    private void userLogin()  {

        //first getting the values
        final String email = userNameEditText.getText().toString();
        final String password = paswdEditText.getText().toString();

        //validating inputs
        if (TextUtils.isEmpty(email)) {
            userNameEditText.setError("Please enter your username");
            userNameEditText.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            paswdEditText.setError("Please enter your password");
            paswdEditText.requestFocus();
            return;
        }
        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("email","kamalvaid@mailinator.com");
            jsonObject.put("password","kamal@123");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObject,  new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                Log.e("Response => ",response.toString());
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.e("Response => ",error.toString());
            }
        })
        { //Code to send parameters to server
            @Override
            protected Map getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<String, String>();
                params.put("email",email);
                params.put("password", password);
                return params;
            }
        };
        queue.add(jsonObjReq);

        /*StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (!obj.getBoolean("error")) {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                                //getting the user from the response
                                JSONObject userJson = obj.getJSONObject("user");

                                //creating a new user object
                                LoginModel user = new LoginModel(
                                        userJson.getString("email"),
                                        userJson.getString("password"));


                                //storing the user in shared preferences
                                // SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);

                                //starting the dashboard activity
                                finish();
                                startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };

        SingletonRequestQueue.getInstance(this).addToRequestQueue(stringRequest);
    }

*/
    }
}


