package com.example.poojaguleria.inspectionapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Button generalInfoButton = (Button)findViewById(R.id.genrl_info_btn);
        generalInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent infoIntent = new Intent(DashboardActivity.this,GeneralInformationActivity.class);
                startActivity(infoIntent);
            }
        });

        Button physicalSecurityButton = (Button)findViewById(R.id.physical_security_btn);
        physicalSecurityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent infoIntent = new Intent(DashboardActivity.this,PhysicalSecurityActivity.class);
                startActivity(infoIntent);
            }
        });

        Button systemSecurityButton = (Button)findViewById(R.id.sys_security_btn);
        systemSecurityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent infoIntent = new Intent(DashboardActivity.this,SystemSecurityActivity.class);
                startActivity(infoIntent);
            }
        });

        Button manPowerButton = (Button)findViewById(R.id.manpower_btn);
        manPowerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent infoIntent = new Intent(DashboardActivity.this,ManpowerActivity.class);
                startActivity(infoIntent);
            }
        });

        Button processProcedureButton = (Button)findViewById(R.id.procedurebtn);
        processProcedureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent infoIntent = new Intent(DashboardActivity.this,PreparationActivity.class);
                startActivity(infoIntent);
            }
        });
    }
}
