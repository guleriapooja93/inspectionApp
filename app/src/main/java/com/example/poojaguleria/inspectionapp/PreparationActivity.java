package com.example.poojaguleria.inspectionapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class PreparationActivity extends AppCompatActivity {

    private List<Questions> questionsList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private QuesListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preparation);

        mRecyclerView = (RecyclerView) findViewById(R.id.question_list);

        setTypProcQuesList();
        setEmerContrlQuesList();
        setLockDwnProQuesList();


    }

    private void setTypProcQuesList() {

        mAdapter = new QuesListAdapter(this,questionsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        Questions questions = new Questions("", "Types of Procedures & Processes");
        questions.setHeader(true);
        questionsList.add(questions);
        questions = new Questions("Q233.Reporting and investigating of suspicious persons, activities and incidents", "");
        questionsList.add(questions);
        questions = new Questions("Q234.Emergency and crisis response plan", "");
        questionsList.add(questions);
        questions = new Questions("Q235.Search of visitors’ belongings and staff lockers", "");
        questionsList.add(questions);
        questions = new Questions("Q236.Dealing with bomb threat calls \n" +
                " If “Yes”, please attach the procedures\n", "");
        questionsList.add(questions);
        questions = new Questions("Q237.Dealing with suspicious mail or parcel \n" +
                " If “Yes”, please attach the procedures\n", "");
        questionsList.add(questions);
        questions = new Questions("Q238.Dealing with unauthorized persons", "");
        questionsList.add(questions);
        questions = new Questions("Q239.Dealing with armed hostage-taking situations \n" +
                "If “Yes”, please attach the procedures\n", "");
        questionsList.add(questions);
        questions = new Questions("Q240.Dealing with unattended objects left in public areas.     \n" +
                " If “Yes”, please attach the procedures\n", "");
        questionsList.add(questions);
        questions = new Questions("Q241.Dealing with crime-related cases (e.g. drugs, weapons, assaults, mischief, thefts, frauds, etc)", "");
        questionsList.add(questions);
        questions = new Questions("Q242.Dealing with public / citizen arrest", "");
        questionsList.add(questions);
        questions = new Questions("Q243.Dealing with injuries and deaths", "");
        questionsList.add(questions);
        questions = new Questions("Q244.Dealing with unruly people", "");
        questionsList.add(questions);
        questions = new Questions("Q245.Dealing with power failure", "");
        questionsList.add(questions);
        questions = new Questions("Q246.Patrol and surveillance", "");
        questionsList.add(questions);
        questions = new Questions("Q247.Security coverage for VIP", "");
        questionsList.add(questions);
        questions = new Questions("Q248.Key control / loss of master key", "");
        questionsList.add(questions);
        questions = new Questions("Q249.In-room safe / safe deposit box", "");
        questionsList.add(questions);
        questions = new Questions("Q250.Access control", "");
        questionsList.add(questions);
        questions = new Questions("Q251.Lost and found", "");
        questionsList.add(questions);
        questions = new Questions("Q252.Dealing with biological threats \n" +
                " If “Yes”, please attach the procedures\n", "");
        questionsList.add(questions);
        questions = new Questions("Q253.Dealing with chemical threats \n" +
                "If “Yes”, please attach the procedures\n", "");
        questionsList.add(questions);
        mAdapter.notifyDataSetChanged();
    }

    private void setEmerContrlQuesList() {

        Questions questions = new Questions("", "Emergency Control Room");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q254.Are hospital layout plans and drawing available in the event of emergency?", "");
        questionsList.add(questions);
        questions = new Questions("Q255.Are contact numbers of emergency services, key personnel and others available in the event of emergency?", "");
        questionsList.add(questions);
        questions = new Questions("Q256.Are the SOPs, Crisis Management Plan, Lockdown Procedures, etc updated and available in the Emergency Control Room?", "");
        questionsList.add(questions);
        questions = new Questions("Q257.Is equipment and other logistics items for use in emergency situation, e.g. Contact List, secure communication lines, telephones, charts, personal protective equipment, stationery available?", "");
        questionsList.add(questions);

        mAdapter.notifyDataSetChanged();
    }

    private void setLockDwnProQuesList() {

        Questions questions = new Questions("", "Lock Down Procedure");
        questions.setHeader(true);

        questionsList.add(questions);
        questions = new Questions("Q258.How are staff alerted of a lockdown?", "");
        questionsList.add(questions);
        questions = new Questions("Q259.Did the lockdown alert system work?", "");
        questionsList.add(questions);
        questions = new Questions("Q260.Did the lockdown occur within the agreed timeframe?", "");
        questionsList.add(questions);
        questions = new Questions("Q261.How long did it take for appropriate personnel to be notified of the activation?", "");
        questionsList.add(questions);
        questions = new Questions("Q262.Are other stakeholders, e.g. tenants, SPF, SCDF, neighbouring / connecting facilities, etc. notified of lockdown?", "");
        questionsList.add(questions);
        questions = new Questions("Q263.Was the sequence of securing doors / windows and cordons correct?", "");
        questionsList.add(questions);
        questions = new Questions("Q264.Were all identified access and egress points secured?", "");
        questionsList.add(questions);
        questions = new Questions("Q265.Were the external locks effective?", "");
        questionsList.add(questions);
        questions = new Questions("Q266.Were the internal locks effective?", "");
        questionsList.add(questions);
        questions = new Questions("Q267.Were the internal corridors within the lockdown perimeter secured?", "");
        questionsList.add(questions);
        questions = new Questions("Q268.Is the locking of doors, etc manually done or through Building Management System?", "");
        questionsList.add(questions);
        questions = new Questions("Q269.Were there any breaches in the lockdown? Were they contained?", "");
        questionsList.add(questions);
        questions = new Questions("Q271.How long does it take for the hospital to carry out the lockdown?", "");
        questionsList.add(questions);
        questions = new Questions("Q272.Has the hospital security team established procedures and protocols with SPF / SCDF for lockdown, e.g. access for emergency responders into the premises that are under lockdown mode?", "");
        questionsList.add(questions);
        mAdapter.notifyDataSetChanged();
    }


}

