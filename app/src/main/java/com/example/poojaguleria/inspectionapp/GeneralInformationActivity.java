package com.example.poojaguleria.inspectionapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class GeneralInformationActivity extends AppCompatActivity {

    TextView nameOfHospitalText;
    TextView addressText;
    TextView dateOfAuditText;
    TextView issuesText;
    TextView objectivesText;
    EditText nameOfHospital;
    EditText address;
    EditText dateOfAudit;
    EditText issues;
    EditText objectives;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_information);

        nameOfHospital = (EditText)findViewById(R.id.edit_text_name);
        address = (EditText)findViewById(R.id.address_edit);
        dateOfAudit = (EditText)findViewById(R.id.date_of_audit_edit);
        issues = (EditText)findViewById(R.id.issues_edit);
        objectives = (EditText)findViewById(R.id.objectives_edit);

        nameOfHospitalText = (TextView) findViewById(R.id.name_text);
        addressText = (TextView)findViewById(R.id.address_text);
        dateOfAuditText = (TextView)findViewById(R.id.date_of_audit_text);
        issuesText = (TextView)findViewById(R.id.issues_text);
        objectivesText = (TextView)findViewById(R.id.objectives_text);


        setTextWatcher();
    }

    private void setTextWatcher() {

        nameOfHospital.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                     if(nameOfHospital.getText().length()>0){
                         nameOfHospitalText.setVisibility(View.VISIBLE);
                     }else{
                         nameOfHospitalText.setVisibility(View.GONE);
                     }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(address.getText().length()>0){
                    addressText.setVisibility(View.VISIBLE);
                }else{
                    addressText.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dateOfAudit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(dateOfAudit.getText().length()>0){
                    dateOfAuditText.setVisibility(View.VISIBLE);
                }else{
                    dateOfAuditText.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        issues.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(issues.getText().length()>0){
                    issuesText.setVisibility(View.VISIBLE);
                }else{
                    issuesText.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        objectives.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(objectives.getText().length()>0){
                    objectivesText.setVisibility(View.VISIBLE);
                }else{
                    objectivesText.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
