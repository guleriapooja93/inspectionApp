package com.example.poojaguleria.inspectionapp;

import java.io.Serializable;

/**
 * Created by Pooja Guleria on 23-07-2018.
 */

public class LoginModel {

    public String email;
    public String password;

    public LoginModel(String enail, String password) {
        this.email = email;
        this.password = password;
    }

    public String getUsername() {
        return email;
    }

    public void setUsername(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
